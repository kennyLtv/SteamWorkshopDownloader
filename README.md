# SteamWorkshopDownloader

Downloads workshop content with SteamCMD and FTP's it to a remote server.

### Prerequisites

Required Packages: glibc.i686 libstdc++.i686 ncftp

## Getting Started

Either clone the repository or just upload the `modInstall.sh`

then chmod for execution. `chmod +x modInstall.sh`

## Usage

Input your FTP info into lines 12-16
```
# FTP Info 
# This will expose your user and password to anyone with file access!!
HOST="10.0.1.98"
USERNAME="ftpuser"
PASSWORD="password"
## Set to the full path of where you want workshop content on remote server
LOCATION="/home/ftpuser/test"
```

Add mod information to be able to download

```
# Mod and App info https://steamdb.info/apps/
MODLIST="1314441674"
APPID="346110"
```


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/kennyLtv/SteamWorkshopDownloader/tags). 

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
