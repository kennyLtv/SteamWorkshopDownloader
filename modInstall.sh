#!/bin/bash
##
#   Steam Workshop Content Downloader
#   Author: Kenneth Lindelof
#   Contact: kenny@kennyl.design
#   Version: 1.0.0
#   GitHub: https://gitlab.com/kennyLtv/SteamWorkshopDownloader
##

# FTP Info 
# This will expose your user and password to anyone with file access!!
HOST="10.0.1.99"
USERNAME="ftpuser"
PASSWORD="password"
## Set to the full path of where you want workshop content on remote server
LOCATION="/home/ftpuser/test"

# Mod and App info https://steamdb.info/apps/
MODLIST="1314441674 878297372"
APPID="346110"

##
# Used by Script
# I don't suggest editing
##
STEAM_CMD="steamcmd.sh"

##
# Actual Script
##
if [ ! -f ./$STEAM_CMD ]; then 
    echo "Downloading SteamCMD..."
    curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf -
    echo "Download complete"
fi

# Break modlist into array
IFS=" "
arr=($MODLIST)
holder=""
for i in ${arr[@]}; do holder="$holder +workshop_download_item $APPID $i "; done;

# SteamCMD won't download mods to forced directory, everything is in the home dir
./$STEAM_CMD +login anonymous $holder +quit

echo "Starting FTP to $HOST"
ncftpput -u $USERNAME -p $PASSWORD -R $HOST $LOCATION ~/Steam/steamapps/workshop/content/$APPID/*
echo "FTP complete"

echo "Removing ~/Steam/steamapps/workshop/content/$APPID"
rm -fr ~/Steam/steamapps/workshop/content/$APPID
echo "Removed ~/Steam/steamapps/workshop/content/$APPID"